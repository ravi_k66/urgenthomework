import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomepageComponent } from './homepage/homepage.component';
import { SampleassignmentComponent } from './sampleassignment/sampleassignment.component';
import { ServicesComponent } from './services/services.component';
import { HowitworksComponent } from './howitworks/howitworks.component';
import { ContactusComponent } from './contactus/contactus.component';
import { SubmithomeworkComponent } from './submithomework/submithomework.component';
import { RouterModule } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { RegisternowComponent } from './registernow/registernow.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomepageComponent,
    SampleassignmentComponent,
    ServicesComponent,
    HowitworksComponent,
    ContactusComponent,
    SubmithomeworkComponent,
    BlogComponent,
    RegisternowComponent,
    ForgetPasswordComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomepageComponent
      },
      {
        path: 'sampleassignment',
        component: SampleassignmentComponent
      },
      {
        path: 'service',
        component: ServicesComponent
      },
      {
        path: 'howitworks',
        component: HowitworksComponent
      },
      {
        path: 'contactus',
        component: ContactusComponent
      },
      {
        path: 'submithomework',
        component: SubmithomeworkComponent
      },
      {
        path: 'blog',
        component:  BlogComponent
      },
      {
        path: 'signUp',
        component:  RegisternowComponent
      },
      {
        path: 'forgetPassword',
        component: ForgetPasswordComponent
      }

    ])
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
