import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmithomeworkComponent } from './submithomework.component';

describe('SubmithomeworkComponent', () => {
  let component: SubmithomeworkComponent;
  let fixture: ComponentFixture<SubmithomeworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmithomeworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmithomeworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
