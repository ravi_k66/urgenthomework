import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleassignmentComponent } from './sampleassignment.component';

describe('SampleassignmentComponent', () => {
  let component: SampleassignmentComponent;
  let fixture: ComponentFixture<SampleassignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleassignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleassignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
